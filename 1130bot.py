#!/usr/bin/env python3

import os
import sys
import signal
import time
import json
import logging
import schedule
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG if os.getenv("DEBUG") is not None else logging.INFO)

dbfile = "1145.json"
db = {"private":[], "group":[], "channel":[]}
bot = None
loop = None
has_answered = {}

survey = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text='👍', callback_data='yes')],
    [InlineKeyboardButton(text='👎', callback_data='no')]
    ])

def sighandler(signum, frame):
    update_db()
    exit(0)

def handle_msg(msg):
    logging.debug("Handling message")
    mime, ctype, cid = telepot.glance(msg)
    if cid not in db[ctype]:
        logging.info("new {}: {}".format(ctype, cid))
        db[ctype].append(cid)
    update_db()

def handle_callback(msg):
    global has_answered
    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
    if 'last_name' in msg['from']:
        user = "{} {}".format(msg['from']['first_name'], msg['from']['last_name'])
    elif 'username' in msg['from']:
        user = "{} (@{})".format(msg['from']['first_name'], msg['from']['username'])
    else:
        user = msg['from']['first_name']
    user_id = msg['from']['id']
    group = msg['message']['chat']['id']
    logging.debug("Handling answer from {}({}) in group {}".format(user, user_id, group))
    if group not in has_answered:
        has_answered[group] = []
    if user_id not in has_answered[group]:
        has_answered[group].append(user_id)
        if query_data == 'yes':
            bot.sendMessage(group, "{}: 👍".format(user))
        elif query_data == 'no':
            bot.sendMessage(group, "{}: 👎".format(user))
    bot.answerCallbackQuery(query_id)

def load():
    logging.info("Loading db: {}".format(dbfile))
    global db
    try:
        with open(dbfile, "r") as dbf:
            db = json.load(dbf)
            if not db:
                db = {"private":[], "group":[], "channel":[]}
                update_db();
    except (FileNotFoundError, json.JSONDecodeError):
        logging.info("No or invalid db found, creating new one")
        update_db()


def update_db():
    global db
    with open(dbfile, "w") as dbf:
        json.dump(db, dbf)

def run_scheduler():
    while True:
        schedule.run_pending()
        time.sleep(60)

def notify():
    global db
    global has_answered
    has_answered = {}
    logging.info("Notifying")
    for uid in db["group"]:
        logging.debug("Notifying: {}".format(uid))
        has_answered[uid] = []
        bot.sendMessage(uid, "1130->am?", reply_markup=survey)

def main(args):
    global bot
    global loop
    if len(args) < 2:
        exit(1)
    logging.info("Starting 1130 bot")
    load()
    signal.signal(signal.SIGINT, sighandler)
    signal.signal(signal.SIGTERM, sighandler)
    schedule.every().monday.at("10:30").do(notify)
    schedule.every().tuesday.at("10:30").do(notify)
    schedule.every().wednesday.at("10:30").do(notify)
    schedule.every().thursday.at("10:30").do(notify)
    schedule.every().friday.at("10:30").do(notify)
    bot = telepot.Bot(args[1])
    loop = MessageLoop(bot, {'chat': handle_msg,
        'callback_query': handle_callback})
    loop.run_as_thread()
    logging.info("Bot started...")
    run_scheduler()


if __name__ == "__main__":
    while True:
        try:
            main(sys.argv)
        except SystemExit:
            logging.info("Catched exit, exiting...")
            exit(0)
        except:
            logging.exception("Something bad happened, recovering in 5 ...")
            update_db()
            time.sleep(5)
            os.execl(sys.argv[0], sys.argv[0], sys.argv[1])
